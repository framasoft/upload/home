/* eslint-disable global-require */
/* eslint-disable import/no-commonjs */

const env = require('./config/env');

/* Build config */
function buildConfig() {
  if (env.mode === 'development') {
    return require('./config/dev')(env);
  }

  if (env.mode === 'preview') {
    return require('./config/preview')(env);
  }

  return require('./config/prod')(env);
}

module.exports = buildConfig();
