prez: |-
  # **Présentation**

  ## C’est Quoi ?

  L’**U**niversité **P**opulaire **L**ibre, **O**uverte, **A**utonome, et **D**écentralisée est un projet protéiforme qui met en œuvre et active des réseaux.

  - un **réseau de lieux**  où on apprend, où on enseigne, où on produit des connaissances. Ces lieux peuvent être numériques ou physiques, éphémères ou pérennes, à distance ou en présence, synchrones ou asynchrones.
  - un **réseau de personnes** qui proposent leurs services, ici ou là, partagent leurs pratiques, échangent leurs expériences.
  - un **réseau de séquences de formation et de documents pédagogiques** libres, produits spécifiquement, ou sélectionnés par curation.

  ## C’est À propos de quoi ?

  De tous les sujets qui intéressent la société, mais plutôt en lien avec le numérique, parce que c’est la génétique de Framasoft, et de l’écologie, parce que c’est inévitable.

  ## C’est Pourquoi ?

  Pour contribuer à rendre la société plus juste et notre monde plus vivable, en misant sur la formation des citoyens par les citoyens.

  ## C’est Qui ?

  Le projet est impulsé et coordonné par l’association [Framasoft](https://framasoft.org) pour le démarrage. Mais on est d'emblée dans une logique en réseau et décentralisée.

  Les copains et copines qui sont déjà dans le mouvement : [Association Scenari](https://scenari.org), [Kelis](https://kelis.fr), [Picasoft](https://picasoft.net)...

  ## C’est Quand ?

  UPLOAD ça n’est pas que du rêve, c’est déjà des petits bouts de réalité… Pour le moment, la production de ressources et le tissage de liens prennent le pas sur la structuration formelle du projet. Il s’agit d’un choix assumé. Cependant, les organisations animant UPLOAD souhaitent avancer au cours des années 2022 et 2023 pour *donner corps* à UPLOAD.

  ## Liste de projets

  - [MOOC CHATONS](https://mooc.chatons.org)
  - [Un annuaire des acteurs et actrices de l’accompagnement au numérique libre](https://framablog.org/2020/09/15/annuaire-des-acteurs-et-actrices-de-laccompagnement-au-numerique-libre/)
  - [Librecours](https://librecours.net/?query=&filters=%5B%7B%22terms%22%3A%7B%22area%22%3A%5B%22UPLOAD%22%5D%7D%7D%5D)
  - [Ressources pédagogiques libres pour le numérique](https://ressources.educpopnum.net/)
  - Vidéos ici et là :
      - [“Écrire, communiquer, collaborer sur le Web”](https://aperi.tube/c/upload),
      - [les lectures du Framablog](https://framatube.org/c/framablog.audio),
      - les [vidéos du MOOC Chatons](https://framatube.org/c/mooc.chatons.1) ou celles de [Libre Culture](https://framatube.org/c/libre.culture).

upload: |-
  # **U. P. L. O. A. D.**

  ## Université.

  Une université c’est d’abord une communauté, réunie autour de lieux publics ou de communs, comme une bibliothèque, des ateliers, des salles de conférence et d’étude. On est au XXIe siècle et le projet UPLOAD en est à ses prémisses, alors pour le moment ce sont surtout des ressources en lignes, à commencer par le MOOC CHATONS, dont le premier volet « Internet, reprendre le contrôle » est ouvert à toutes et tous sur *mooc.chatons.org*. Il s’agit aussi de parcours de formation libres disponibles sur *librecours.net*. Nous laissons aussi traîner de plus en plus de ressources audiovisuelles libres sur Peertube. Enfin sur *educpopnum.net* nous avons commencé un annuaire de ressources libres un peu comme le fait Framalibre pour les logiciels libres.

  ## Populaire.

  Bon, des  MOOC, il y en existe déjà pas mal, alors pourquoi en rajouter ? D’abord, nous avons vu au moment du confinement que le compte n’y était pas. C’est d’abord parce que la pratique de l’enseignement en ligne n’est pas habituelle pour toutes et tous, surtout en dehors du champ de la formation professionnelle ou de l’auto-formation en informatique. On a vu des profs chercher à refaire ce qu’ils et elles savaient faire en présentiel, malgré la distance, quitte à dégrader. Et on a vu la dépendance aux géants du web étatsuniens. Notre ambition est d’aider au développement de pratiques d’éducation populaire, en parallèle des réseaux professionnels ou académiques, pour permettre à chacune et chacun de se former *et* de former.

  ## Libre.

  Pour rendre possible une pratique courante d’éducation populaire, on a besoin de bonnes volontés, d’un réseau Internet qui fonctionne et de ressources libres. Les bonnes volontés, on mise dessus. Le réseau, on en a plus qu’il n’en faut. L’enjeu est de créer des communs, des ressources que chacun peut utiliser, modifier, améliorer et reverser, des ressources éducatives libres. Alors dans UPLOAD l’idée est de faire des contenus sous des vraies licences libres (CC BY ou CC BY-SA typiquement) et de donner les moyens à chacun⋅e de récupérer les sources, les modifier et les repartager.

  ## Ouverte.

  En informatique notamment, il y a déjà des écoles accessibles sans exigence de scolarité antérieure. On peut y entrer sans diplômes et, quand elles ne sont pas gratuites, on peut trouver assez facilement des financements via l’aide publique. Et si la formation en informatique est en effet plutôt ouverte aujourd’hui, ainsi que dans les autres domaines où les boîtes embauchent, l’enjeu est aussi de se former sur des thématiques qui intéressent moins le capitalisme à court terme. Pour s’inspirer un peu, on peut regarder du côté des Céméa et des Universités Populaires : de la philo, de l’analyse de cycle de vie des objets, du bricolage, un peu de médecine, pourquoi pas ? Et tout ce dont on aura besoin demain. Dans un monde promis au bouleversement, ce qui compte c’est moins ce qu’on sait enseigner pour le moment, que comment apprendre ensemble demain ce qu’on ne sait pas encore aujourd’hui.

  ## Autonome.

  C’est l’occasion de retrouver un peu l’utopie d’un Internet ouvert à tous, transversal, qui met chacune et chacun à égalité pour apprendre et enseigner sur le réseau. La formation tout au long de la vie, ça va bien avec l’enseignement tout au long de la vie. Apprends-moi à programmer, je t’apprendrais à jouer de la guitare. Wikipédia est bien sûr un morceau bien vivant de cette utopie. Mais là où l’encyclopédie peut viser l’universel, l’éducation ne peut viser que le local, le très local. Un cours c’est toujours ici et maintenant, entre des personnes singulières. Il faut de l’humain, de la communication, des échanges, des espaces pour des rencontres.

  ## Décentralisée.

  Voilà, on y arrive. L’enjeu *in fine* est là, pas une usine à gaz, pas une Grande école De Framasoft, mais des tas de petites structures implantées dans des tas de petites villes, avec une organisation en archipel comme on les aime. Alors UPLOAD ce serait une fédération de plateformes en ligne et de lieux physiques. Chacun aura accès à tous les contenus, y compris pour les copier et les modifier, et pourra proposer des sessions de formation. On y trouvera sûrement un peu de tout, encore beaucoup d’informatique, tant qu’il y aura des ordinateurs et un peu de l’électricité, mais aussi des humanités, parce que c’est important également d’essayer de comprendre les humains, et puis de l’écologie. On ne sait pas encore exactement ce que ça veut dire, beaucoup de choses, mais on commence à savoir par où commencer. Essayer de mieux comprendre comment la Terre fonctionne, participer à évaluer nos impacts, s’autonomiser, jardiner ou apprendre à réparer.

  On y trouvera ce que chacun·e y mettra, ce qui lui semblera important à transmettre et à recevoir.
story: |-
  # **Histoire d’UPLOAD**

  Camille et Sasha marchent côte à côté. Ils ne se tiennent pas par la main, pourtant, Sasha le voudrait bien.

  — Tu vas en cours ce matin ? demande Camille avec un petit sourire qui semble ajouter, même si je sais bien que t’y vas pas souvent.

  — Je sais pas, c’est quoi au menu ?

  — Initiation à la programmation, on va faire du JavaScript.

  — On a pas déjà fait ça au trimestre dernier ?

  — C’est un on qui t’inclut pas, hein ? Je t’y ai pas vu souvent en cours le trimestre dernier ! répond Camille avec malice. Non, on a surtout vu comment se servir de l’ordi, gérer son système, et on a vu comme écrire et publier du contenu sur le Web via les Rasp de l’asso. On a fait un tout petit peu de JS, mais…

  — de JS ?

  — JavaScript. T’es ~~largué~~ à la rue ! Bref, là on va vraiment apprendre à écrire des programmes, c’est la partie que je préfère. À la fin on va programmer un petit robot. C’est César du repair café qui l’a fabriqué.

  — Je pense que je vais aller au jardin plutôt.

  — Y a rien à y faire en hiver !

  — Si, si, y a toujours des trucs à faire. Y a la serre à ranger. Et je vais finir de pailler, on a récolté les dernières courges et il reste des coins où la terre est à nu. Le paillage, ça prépare un peu d’humus pour le printemps, et puis ça limite les adventices.

  — Les adventices ? T’en connais des mots quand tu veux !

  — Les herbes indésirables.

  — Ha, les mauvaises herbes…

  — C’est toi la mauvaise herbe ! la coupe-t-il en riant. On dit plus comme ça. L’herbe elle est mauvaise que par rapport à ce qu’on veut faire nous les humains à un moment donné avec, mais pas en général. Elle rend des services aussi. C’est important les mots, ça force à réfléchir un peu plus. Ça construit notre représentation de la réalité, même.

  — Merci pour la leçon.

  Camille est un peu piquée, mais finalement elle se dit que Sasha est pas si ballot qu’il veut bien le laisser paraître, il doit être plus assidu aux cours de philo. Et sur son jardin, il est à fond, elle trouve ça mignon.

  — Tu vois les orties, c’est la plaie, hein, ça pique sa race, ça se faufile partout, tu galères à t’en débarrasser. Et ben tu peux faire des tas de trucs avec, de l’engrais, du désherbant, même ça se bouffe. J’ai essayé, je me suis cramé la gorge et j’ai eu l’impression d’avoir une otite pendant trois jours, mais parait que je m’y suis mal pris, qu’il faut manger que les jeunes pousses. Je réessaierai.

  Et puis, Camille, elle se dit que quand elle reprogrammera le monde, il faudra bien des types comme lui pour lui préparer à manger. Elle rêvasse à elle vautrée derrière son PC et Sasha à la cuisine en train de lui préparer une omelette fromage et orties qui pique un peu à l’intérieur des oreilles.

  — On se retrouve cet aprem alors, tu vas au wushu ?

  Ils arrivent à l’école. C’est des anciens bureaux de la ville qui devaient être détruits, trop vieux, trop à l’écart. Finalement, ils les ont filés à l’asso. Petit à petit chaque nouvelle promo rénove, de l’isolation surtout. Avec du chanvre qu’on fait pousser nous mêmes. Y a toujours plein de volontaires pour faire pousser du chanvre. C’est pas du style second empire, plutôt un gros cube décrépit planté au milieu de nulle part, et en hiver, avec les champs rasés de frais et la forêt sans feuilles, c’est un peu triste. Mais le méga-tag plein de couleurs qui tartine toute la façade donne la patate quand tu t’approches. Il annonce dans un style à la fois foutraque et fier : UPLOAD. Université Populaire Libre Ouverte Autonome Décentralisée. Et en plus petit en dessous : Technologie, Écologie, Humanité.

  Pourtant, Sasha fait un peu la gueule, comme à chaque fois qu’ils arrivent et qu’il n’a pas réussi à lui parler d’autre chose que de son jardin à Camille. Je t’aime Camille ! Voilà. Ben non. À tout à l’heure Camille.

  ## D’autres trucs à lire

  - Framasoft, 2019a, « [MOOC CHATONS : un parcours pédagogique pour favoriser l’émancipation numérique](https://framablog.org/2019/12/04/mooc-chatons-un-parcours-pedagogique-pour-favoriser-lemancipation-numerique/) », Framablog, CC BY-SA.
  - Framasoft, 2020. « [Un librecours pour mieux contribuer à la culture libre](https://framablog.org/2020/03/25/un-librecours-pour-mieux-contribuer-a-la-culture-libre/) ». Framablog, CC BY-SA.
  - stph, 2021, « [Librecours propose une initiation à la programmation informatique](https://linuxfr.org/news/librecours-propose-une-initiation-a-la-programmation-informatique) », Linuxfr.org, édité par Ysabeau et Benoît Sibaud, CC BY‑SA.
  - Gosset, Pierre-Yves. « [La place du numérique à l’école est à l’image de la place de l’école dans la société](https://cfeditions.com/sans-ecole/.) ». In L’École sans école : Ce que le confinement nous dit de l’éducation, C&F Éditions, 2021.
  - Hubert Guillaud. 2020. [Coincés dans Zoom (1/4) : de la vidéoconférence en ses limites](http://www.internetactu.net/2020/11/20/coinces-dans-zoom-13-de-la-videoconference-en-ses-limites/.) Coincés dans Zoom (1/4). in InternetActu.net.
  - Fengchun Miao, Sanjaya Mishra, Dominic Orr et Ben Janssen, 2020, « [Lignes directrices pour l’élaboration des politiques sur les ressources éducatives libres](https://unesdoc.unesco.org/ark:/48223/pf0000373887) », UNESCO et Commonwealth of Learning, CC BY-SA.
  - Ivan Illich, 1971, « Une société sans école », Seuil.
  - Framasoft, 2019b, « [Archipélisation : comment Framasoft conçoit les relations qu’elle tisse](https://framablog.org/2019/12/10/archipelisation-comment-framasoft-concoit-les-relations-quelle-tisse/) », Framablog, CC BY-SA.
